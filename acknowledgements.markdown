This software is modified from its original form.  The original acknowledgements file contents is below this section for reference.

In addition to the original acknowledgements noted below, Unity Technologies has made the following changes:

# Software distributed with/compiled into NSubstitute

## Mono Runtime v3.4.0.204 [http://www.mono-project.com]
Some files have been copied from v3.4.0.204 [https://github.com/mono/mono/tree/mono-3.4.0.204] of the Mono Runtime and modified in order to add some Mono support to NSubstitute.  The modified versions of these files from the Mono runtime are included with this version of NSubstitute.  The Mono runtime is released under the MIT/X11 license [http://opensource.org/licenses/MIT].

## Castle.DynamicProxy [http://castleproject.org]
This version of NSubstitute bundles Castle.DynamicProxy [https://github.com/castleproject/Core/tree/8d5eacbcd21aa13c2e91506dfa5a6ad16ef2d553/src/Castle.Core/DynamicProxy].  Like Castle.Core, Castle.DynamicProxy is released under an Apache License, Version 2.0 [http://www.apache.org/licenses/LICENSE-2.0.html].

## Castle.Core [http://castleproject.org]
The original version of NSubstitute comes with dynamically linked Castle.Core library.  This version of NSubstitute includes the Castle.Core source from the Castle Project [http://www.castleproject.org/], retreived from the upstream Castle.Core source [https://github.com/castleproject/Core/tree/8d5eacbcd21aa13c2e91506dfa5a6ad16ef2d553/src/Castle.Core] in order to allow for modifications that create a bettery library loading experience.  Castle.Core is released under the Apache License, Version 2.0 [http://www.apache.org/licenses/LICENSE-2.0.html].

***

The aim of this file is to acknowledge the software projects that have been used to create NSubstitute, particularly those distributed as Open Source Software. They have been invaluable in helping us produce this software.

# Software distributed with/compiled into NSubstitute

## Castle.Core
NSubstitute is built on the Castle.Core library, particularly Castle.DynamicProxy which is used for generating proxies for types and intercepting calls made to them so that NSubstitute can record them. 

Castle.Core is maintained by the Castle Project [http://www.castleproject.org/] and is released under the Apache License, Version 2.0 [http://www.apache.org/licenses/LICENSE-2.0.html].

# Software used to help build NSubstitute

## NUnit [http://www.nunit.org/]
NUnit is used for coding and running unit and integration tests for NSubstitute. It is distributed under an open source zlib/libpng based license [http://www.opensource.org/licenses/zlib-license.html].

## Rhino Mocks [http://www.ayende.com/projects/rhino-mocks.aspx]
Used for mocking parts of the NSubstitute mocking framework for testing. It is distributed under the BSD license [http://www.opensource.org/licenses/bsd-license.php].

## Moq [http://moq.me/]
Moq is not used in NSubstitute, but was a great source of inspiration. Moq pioneered Arrange-Act-Assert (AAA) mocking syntax for .NET, as well as removing the distinction between mocks and stubs, both of which have become important parts of NSubstitute. Moq is available under the BSD license [http://www.opensource.org/licenses/bsd-license.php].

## NuPack [http://nupack.codeplex.com/]
Used for packaging NSubstitute for distribution as a nu package. Distributed under the Apache License, Version 2.0 [http://www.apache.org/licenses/LICENSE-2.0.html].

## Jekyll [http://jekyllrb.com/]
Static website generator written in Ruby, used for NSubstitute's website and documentation. Distributed under the MIT license [http://www.opensource.org/licenses/bsd-license.php].

## SyntaxHighlighter [http://alexgorbatchev.com/SyntaxHighlighter/]
Open source, JavaScript, client-side code highlighter used for highlighting code samples on the NSubstitute website. Distributed under the MIT License [http://en.wikipedia.org/wiki/MIT_License] and the GPL [http://www.gnu.org/copyleft/lesser.html].

## Ruby (and libraries) [http://www.ruby-lang.org]
Ruby is used for NSubstitute's build. The rake library is used to run the build; custom Ruby code is used to extract and test code samples from documentation; rspec is used to test that Ruby code; Markdown is used for documentation; gems for installing all this stuff; RubyInstaller for Windows and DevKit for interop with Windows; and much more. Ruby is awesome, as are the many projects contributed to the Ruby community that make developers' lives awesome.

Ruby is distributed under the Ruby license [http://www.ruby-lang.org/en/LICENSE.txt]. The libraries mentioned are available under a variety of licenses. Please see their websites for more information.

## Microsoft .NET Framework [http://www.microsoft.com/net/]
NSubstitute is coded in C# and compiled using Microsoft .NET. It can also run and compile under Mono [http://www.mono-project.com], an open source implementation of the open .NET standards for C# and the CLI.

Microsoft's .NET Framework is available under a EULA (and possibly other licenses like MS Reference Source License).
Mono is available under four open source licenses for different parts of the project (including MIT/X11, GPL, MS-Pl). These are described on the project site [http://www.mono-project.com/Licensing].

## Microsoft Ilmerge [http://research.microsoft.com/en-us/people/mbarnett/ilmerge.aspx]
Used for combining assemblies so NSubstitute can be distributed as a single DLL. Available for use under a EULA as described on the ilmerge site.

## Microsoft Reactive Extensions for .NET (Rx) [http://msdn.microsoft.com/en-us/devlabs/ee794896]
Used to provide .NET 3.5 with some of the neat concurrency helper classes that ship with out of the box with .NET 4.0. Distributed under a EULA [http://msdn.microsoft.com/en-us/devlabs/ff394099].

## 7-Zip [http://www.7-zip.org/]
7-zip is used to ZIP up NSubstitute distributions as part of the automated build process. Distributed under a mixed GNU LGPL / unRAR licence [http://www.7-zip.org/license.txt].

# Other acknowledgements

## Software developers
Yes, you! To everyone who has tried to get better at the craft and science of programming, especially those of you who have talked, tweeted, blogged, screencasted, and/or contributed software or ideas to the community.

No software developers were harmed to any significant extent during the production of NSubstitute, although some had to get by on reduced sleep.

